package vsupport

import (
	"fmt"
	"runtime"
	"time"

	"github.com/mackerelio/go-osstat/memory"
)

var startTime = time.Now()

// Stats returns Go statistics
func Stats() []string {
	var stats []string
	var goStats runtime.MemStats
	runtime.ReadMemStats(&goStats)
	osStats, err := memory.Get()
	if err == nil {
		stats = []string{
			fmt.Sprintf("Memory total: %s", prettyBytes(osStats.Total)),
			fmt.Sprintf("Memory available: %s", prettyBytes(osStats.Available)),
			fmt.Sprintf("Memory used: %s", prettyBytes(osStats.Used)),
			fmt.Sprintf("Memory cached: %s", prettyBytes(osStats.Cached)),
			fmt.Sprintf("Memory free: %s", prettyBytes(osStats.Free)),
		}
	}
	return append(stats,
		fmt.Sprintf("Number of logical CPUs usable by the current process: %d", runtime.NumCPU()),
		fmt.Sprintf("Number of goroutines that currently exist: %d", runtime.NumGoroutine()),
		fmt.Sprintf("Total memory obtained from the OS: %s", prettyBytes(goStats.Sys)),
		fmt.Sprintf("Bytes of physical memory returned to the OS: %s", prettyBytes(goStats.HeapReleased)),
		fmt.Sprintf("Cumulative max bytes allocated on the heap (will not decrease): %s", prettyBytes(goStats.TotalAlloc)),
		fmt.Sprintf("Currently allocated number of bytes on the heap: %s", prettyBytes(goStats.Alloc)),
		fmt.Sprintf("Bytes of heap memory obtained from the OS: %s", prettyBytes(goStats.HeapSys)),
		fmt.Sprintf("Bytes in idle (unused) spans: %s", prettyBytes(goStats.HeapIdle)),
		fmt.Sprintf("Bytes in in-use spans: %s", prettyBytes(goStats.HeapInuse)),
		fmt.Sprintf("Bytes of physical memory returned to the OS: %s", prettyBytes(goStats.HeapReleased)),
		fmt.Sprintf("Number of allocated heap objects: %d", goStats.HeapObjects),
		fmt.Sprintf("Cumulative count of heap objects allocated: %d", goStats.Mallocs),
		fmt.Sprintf("Cumulative count of heap objects freed: %d", goStats.Frees),
		fmt.Sprintf("Number of live objects: %d", goStats.Mallocs-goStats.Frees),
		fmt.Sprintf("Target heap size of the next GC cycle: %s", prettyBytes(goStats.NextGC)),
		fmt.Sprintf("Total GC pauses (in nanoseconds) since the app has started: %d", goStats.PauseTotalNs),
		fmt.Sprintf("Number of completed GC cycles: %d", goStats.NumGC),
		fmt.Sprintf("Uptime: %v", time.Since(startTime)),
	)
}

func prettyBytes(bytes uint64) string {
	const unit uint64 = 1024
	if bytes < unit {
		return fmt.Sprintf("%d B", bytes)
	}
	div, exp := unit, 0
	for n := bytes >> 10; n >= unit; n >>= 10 {
		div <<= 10
		exp++
	}
	return fmt.Sprintf("%.2f %ciB", float64(bytes)/float64(div), "KMGTPE"[exp])
}
