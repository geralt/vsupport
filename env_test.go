package vsupport

import (
	"os"
	"testing"
)

func TestStringOption(t *testing.T) {
	if err := os.Setenv("opt", "abc"); err != nil {
		t.Error("Failed to set environment string option.")
	}

	v := StringOption("opt", "")
	if v != "abc" {
		t.Error("String option does not match its expected value \"abc\".")
	}

	v = StringOption("foo", "default")
	if v != "default" {
		t.Error("String option does not match its default value.")
	}
}

func TestBoolOption(t *testing.T) {
	values := map[string]bool{
		"":      false,
		"0":     false,
		"false": false,
		"FALSE": false,
		"FaLSe": false,
		"1":     true,
		"true":  true,
		"TRUE":  true,
		"TRuE":  true,
	}
	var v bool
	for optionValue, expectedValue := range values {
		if err := os.Setenv("opt", optionValue); err != nil {
			t.Error("Failed to set environment bool option.")
		}

		v = BoolOption("opt", false)
		if v != expectedValue {
			t.Errorf("Bool option \"%s\" does not match its value \"%v\".", optionValue, expectedValue)
		}
	}

	v = BoolOption("foo", true)
	if v != true {
		t.Error("Bool option does not match its default value.")
	}
}

func TestIntOption(t *testing.T) {
	if err := os.Setenv("opt", "-123"); err != nil {
		t.Error("Failed to set environment integer option.")
	}

	v := IntOption("opt", 0)
	if v != -123 {
		t.Error("Integer option does not match its expected value 123.")
	}

	v = IntOption("foo", 10)
	if v != 10 {
		t.Error("Integer option does not match its default value.")
	}

	_ = os.Setenv("opt", "abc")
	v = IntOption("opt", 3)
	if v != 3 {
		t.Error("Invalid integer option does not match its default value.")
	}
}
