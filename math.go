package vsupport

import "time"

func PowDuration(interval time.Duration, exponent int) time.Duration {
	return time.Duration(Pow(int(interval), exponent))
}

func Pow(base int, power int) int {
	result := 1
	for power > 0 {
		if power&1 == 1 {
			result *= base
		}
		base = base * base
		power = power >> 1
	}
	return result
}
