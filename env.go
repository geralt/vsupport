package vsupport

import (
	"os"
	"strconv"
	"strings"
)

func StringOption(option string, defaultValue string) string {
	value := os.Getenv(option)
	if value == "" {
		value = defaultValue
	}
	return value
}

func BoolOption(option string, defaultValue bool) bool {
	value := os.Getenv(option)
	if value == "" {
		return defaultValue
	}
	if strings.ToLower(value) == "false" || value == "0" {
		return false
	}
	return true
}

func IntOption(option string, defaultValue int) int {
	value, err := strconv.Atoi(os.Getenv(option))
	if err != nil {
		return defaultValue
	}
	return value
}
