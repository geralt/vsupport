package vsupport

import (
	"testing"
	"time"
)

func TestPowDuration(t *testing.T) {
	var repeatInterval, interval, i time.Duration
	for i = 1; i < 10; i++ {
		repeatInterval = i
		interval = i
		exponent := 5
		for n := 1; n < exponent; n++ {
			interval = interval * repeatInterval
		}

		powInterval := PowDuration(repeatInterval, exponent)

		if interval != powInterval {
			t.Errorf("Pow result do not match. Expected %d, got %d", interval, powInterval)
		}
	}
}

func BenchmarkPowLoop(b *testing.B) {
	var repeatInterval, interval time.Duration
	repeatInterval = 3
	interval = 3
	exponent := 10
	for i := 0; i < b.N; i++ {
		for n := 1; n < exponent; n++ {
			interval = interval * repeatInterval
		}
	}
}

func BenchmarkPowFast(b *testing.B) {
	for i := 0; i < b.N; i++ {
		PowDuration(3, 10)
	}
}
